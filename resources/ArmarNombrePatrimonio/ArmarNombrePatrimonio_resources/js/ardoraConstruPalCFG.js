//Creado con Ardora - www.webardora.net
//bajo licencia Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
//para otros usos contacte con el autor
var timeAct=360; timeIni=360; timeBon=0;
var successes=0; successesMax=5; attempts=0; attemptsMax=1;
var score=0; scoreMax=5; scoreInc=1; scoreDec=1
var typeGame=0;
var tiTime=true;
var tiTimeType=2;
var tiButtonTime=true;
var textButtonTime="Comenzar";
var tiSuccesses=false;
var tiAttempts=false;
var tiScore=true;
var startTime;
var colorBack="#FFFDFD"; colorButton="#91962F"; colorText="#000000"; colorSele="#FF8000";
var goURLNext=false; goURLRepeat=false;tiAval=false;
var scoOk=0; scoWrong=0; scoOkDo=0; scoWrongDo=0; scoMessage=""; scoPtos=10;
var fMenssage="Verdana, Geneva, sans-serif";
var fActi="Verdana, Geneva, sans-serif";
var fEnun="Tahoma, Geneva, sans-serif";
var timeOnMessage=5; messageOk="¡Muy bien!"; messageTime="¡Se acabó el tiempo!"; messageError="¡Volvé a intentarlo!"; messageErrorG="¡Volvé a intentarlo!"; messageAttempts=""; isShowMessage=false;
var urlOk=""; urlTime=""; urlError=""; urlAttempts="";
var goURLOk="_blank"; goURLTime="_blank"; goURLAttempts="_blank"; goURLError="_blank"; 
borderOk="#008000"; borderTime="#FF0000";borderError="#FF0000"; borderAttempts="#FF0000";
var cp_pal=["UVVFQlJBREEgREUgSFVNQUhVQUNB","U0lTVEVNQSBWSUFMIEFORElOTw==","TUlTSU9ORVMgSkVTVcONVElDQVM=","Q1VFVkEgREUgTEFTIE1BTk9T","TUFOWkFOQSBZIEVTVEFOQ0lBUyBKRVNVw41USUNBUw=="];var cp_ima=["","","","",""];var cp_mp3=["","","","",""];var cp_ogg=["","","","",""];var cp_que=["IkVzdGUgc2l0aW8gc2UgZXh0aWVuZGUgYSBsbyBsYXJnbyBkZSB1biBpbXBvcnRhbnRlIGl0aW5lcmFyaW8gY3VsdHVyYWwsIGVsIENhbWlubyBkZWwgSW5jYSwgcXVlIHNpZ3VlIGVsIGN1cnNvIGRlbCBSw61vIEdyYW5kZSB5IHN1IGVzcGVjdGFjdWxhciB2YWxsZSwgZGVzZGUgc3UgbmFjaW1pZW50byBlbiBlbCBhbHRpcGxhbm8gZGVzw6lydGljbyB5IGZyw61vIGRlIGxvcyBBbHRvcyBBbmRlcyBoYXN0YSBzdSBjb25mbHVlbmNpYSBjb24gZWwgUsOtbyBMZW9uZS4i","IlNlIHRyYXRhIGRlIHVuYSB2YXN0YSByZWQgdmlhcmlhIGRlIHVub3MgMzAuMDAwIGtpbMOzbWV0cm9zIGNvbnN0cnVpZGEgYSBsbyBsYXJnbyBkZSB2YXJpb3Mgc2lnbG9zIHBvciBsb3MgaW5jYXMsIGNvbiB2aXN0YXMgYSBmYWNpbGl0YXIgbGFzIGNvbXVuaWNhY2lvbmVzLCBsb3MgdHJhbnNwb3J0ZXMgeSBlbCBjb21lcmNpbywgeSB0YW1iacOpbiBjb24gZmluZXMgZGVmZW5zaXZvcy4i","IlJ1aW5hcyB1YmljYWRhcyBlbiBlbCBjb3JhesOzbiBtaXNtbyBkZSBsYSBzZWx2YSB0cm9waWNhbCB5IGNvbnN0cnVpZGFzIGVuIHRlcnJpdG9yaW8gZ3VhcmFuw60gZHVyYW50ZSBsb3Mgc2lnbG9zIFhWSUkgeSBYVklJSSwgZXN0YXMgbWlzaW9uZXMgc2UgY2FyYWN0ZXJpemFuIHBvciBzdSB0cmF6YWRvIGVzcGVjw61maWNvIHkgc3UgZGVzaWd1YWwgZXN0YWRvIGRlIGNvbnNlcnZhY2nDs24uIg==","IkVzdGUgcGF0cmltb25pbyBhbGJlcmdhIHVuIGNvbmp1bnRvIGV4Y2VwY2lvbmFsIGRlIGFydGUgcnVwZXN0cmUsIGVqZWN1dGFkbyBlbnRyZSBsb3MgYcOxb3MgMTMuMDAwIHkgOS41MDAgYS5DLiBBZGVtw6FzIHBvc2VlIG51bWVyb3NhcyByZXByZXNlbnRhY2lvbmVzIGRlIGVzcGVjaWVzIGHDum4gdml2YXMgZGUgbGEgZmF1bmEgbG9jYWwsIHkgbcOhcyBjb25jcmV0YW1lbnRlIGRlIGd1YW5hY29zIChsYW1hIGd1YW5pY29lKS4i","IkZ1ZSB1bm8gZGUgbG9zIG7DumNsZW9zIGRlIGRlIGxhIGFudGlndWEgcHJvdmluY2lhIGRlbCBQYXJhZ3VheSBkZSBsYSBDb21wYcOxw61hIGRlIEplc8O6cywgY29tcHJlbmRlIGxhIHVuaXZlcnNpZGFkLCBsYSBpZ2xlc2lhLCBsYSByZXNpZGVuY2lhIGRlIGxvcyBwYWRyZXMgamVzdWl0YXMgeSBlbCBjb2xlZ2lvIE1vbnRzZXJyYXQuIg=="];var cp_num=[21,19,19,18,30];var cp_alt=["","","","",""];
var wordsGame="QXJtYXJOb21icmVQYXRyaW1vbmlv"; wordsStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
function giveZindex(typeElement){var valueZindex=0; capas=document.getElementsByTagName(typeElement);
for (i=0;i<capas.length;i++){if (parseInt($(capas[i]).css("z-index"),10)>valueZindex){valueZindex=parseInt($(capas[i]).css("z-index"),10);}}return valueZindex;}
var au="";var cp=[];var letters=[];var posAns=0;var lettersId=[];var lettersX=[];var lettersY=[];var lettersAns=[];var answers=[];var indexGame=1;var numle=5; var fillLetter="ABCDEFGHIJKLMNÑOPQRSTUVWXYZÁÉÍÓÚÜ";var jobindex=[];
